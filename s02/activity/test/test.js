const { factorial, div_check } = require('../src/util.js');

// Gets the expect and assert functions from chai to be used
const {expect, assert} = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" accepts two parameters
	// string explaining what the test should do
	// callback function which contains the actual test
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		// "expect" - returning expected and actual value
		expect(product).to.equal(120);
	})

	// it('test_fun_factorial_5!_is_120', () => {
	// 	const product = factorial(5);
	// 	assert.equal(product, 120)
	// })

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		// "assert" - checking if function is return correct results
		assert.equal(product, 1);
	})

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		assert.equal(product, 3628800);
	})

})

describe('test_divisibility_by_5_or_7', () => {

	it('test_100_is_divisible_by_5_or_7', () => {
		const product = div_check(100);
		assert.equal(product, true);
	})

	it('test_21_is_divisible_by_5_or_7', () => {
		const product = div_check(21);
		assert.equal(product, true);
	})

	it('test_24_is_not_divisible_by_5_or_7', () => {
		const product = div_check(24);
		assert.equal(product, false);
	})

	it('test_88_is_not_divisible_by_5_or_7', () => {
		const product = div_check(88);
		assert.equal(product, false);
	})

	it('test_91_is_divisible_by_5_or_7', () => {
		const product = div_check(91);
		assert.equal(product, true);
	})
})