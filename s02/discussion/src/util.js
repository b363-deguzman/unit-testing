function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
	// 5 * 4 * 3 * 2 * 1 = 120
}

module.exports = {
	factorial: factorial
} 
